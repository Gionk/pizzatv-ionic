import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { OnInit } from '@angular/core';
import { Show } from '../../models/show';
import { PizzaServerService } from '../../services/pizza-server.service';
import { ToastService } from '../../services/toast.service';
import { SettingService } from '../../services/settings.service';

import { DetailPage } from '../detail/detail';
import { SettingsPage } from '../settings/settings';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
  providers: [PizzaServerService, ToastService, SettingService]
})
export class HomePage implements OnInit {

  // parameters
  target: string;

  // variables
  toplist: { [target: string] : Show[]; } = {};
  searchResults: { [target: string] : Show[]; } = {};

  // settings
  disableFastSearch: boolean;
  simplifiedInterface: boolean;
  showReccomanded: boolean;

  constructor(public navCtrl: NavController, private navParams: NavParams,
              private pizzaService: PizzaServerService, public toastService: ToastService,
              private settingService: SettingService) {
    // pass
    this.toplist['tv'] = []; this.toplist['movie'] = [];
    this.searchResults['tv'] = []; this.searchResults['movie'] = [];
  }

  async ngOnInit(): Promise<void> {
    // Check for baseurl
    let baseurl = await this.settingService.getSetting('baseurl');

    if (!baseurl) {
      // Go to settings page
      this.navCtrl.push(SettingsPage);
    }
  }

  async getTopLists() {
    let page = Math.floor(this.toplist[this.target].length / 20) + 1;
    try {
      let newItems = await this.pizzaService.getTopList(this.target, page);
      this.toplist[this.target] = this.toplist[this.target].concat(newItems);
    } catch (error) {
      this.toastService.showTextToast('Impossibile connettersi al server', 'error');
      console.log(error);
    }
  }

  // Runs when the page is about to enter and become the active page.
  async ionViewWillEnter(): Promise<void> {
    // Update settings
    this.disableFastSearch = await this.settingService.getSetting('fastSearch');
    this.simplifiedInterface = await this.settingService.getSetting('simplifiedInterface');
    this.showReccomanded = await this.settingService.getSetting('showReccomanded');

    // Set the target ('movie', 'tv') for the search page
    this.target = this.navParams.data;

    // get toplist
    this.getTopLists();
  }

  search(searchText: string): void {
    this.pizzaService.search(searchText, this.target).then(shows => {
      // remove results without a poster
      let filteredShows: Show[] = shows.filter(x => x.poster_path);

      // sort by popularity
      filteredShows = filteredShows.sort((a, b) => Number(b.popularity) - Number(a.popularity));

      if (filteredShows.length == 0){
        this.toastService.showTextToast('Non ci sono risultati');
      }

      this.searchResults[this.target] = filteredShows;
    });
  }

  getItems(ev: any) {
    // set val to the value of the searchbar
    let val = ev.target.value;

    // if the value is an empty string don't filter the items
    if (val && val.trim() != '') {
      if (this.disableFastSearch == false) this.search(val);
    } else {
      this.searchResults[this.target] = [];
    }
  }

  goToDetail(selectedShow: Show): void {
    console.log("selectedShow", selectedShow);
    this.navCtrl.push(DetailPage, {show: selectedShow, target: this.target});
  }

  doInfinite(): Promise<any> {
    return this.getTopLists();
  }

}
