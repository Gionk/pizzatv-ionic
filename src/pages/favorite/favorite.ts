import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { ToastService } from '../../services/toast.service';
import { FavoriteService } from '../../services/favorite.service';
import { SettingService } from '../../services/settings.service';


import { Show } from '../../models/show';
import { DetailPage } from '../detail/detail';

@Component({
  selector: 'page-favorite',
  templateUrl: 'favorite.html',
  providers: [ToastService, FavoriteService, SettingService]
})
export class FavoritePage {

  shows: Show[] = []; // favorite lists
  simplifiedInterface: boolean;

  constructor(public navCtrl: NavController, public toastService: ToastService,
              private favoriteService: FavoriteService, private settingService: SettingService) {

  }

  // Runs when the page is about to enter and become the active page.
  async ionViewWillEnter(): Promise<void> {
    this.favoriteService.getFavoriteList().then(shows => this.shows = shows);
    this.simplifiedInterface = await this.settingService.getSetting('simplifiedInterface');
  }

  goToDetail(selectedShow: Show): void {
    this.navCtrl.push(DetailPage, {show: selectedShow, target: selectedShow.target});
  }
}
