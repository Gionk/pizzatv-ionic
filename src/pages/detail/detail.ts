import { Component, OnInit, ViewChild } from '@angular/core';
import { NavController, NavParams, ActionSheetController, LoadingController, Content, Platform, Loading } from 'ionic-angular';

// Custom Services
import { PizzaServerService } from '../../services/pizza-server.service';
import { ParserService } from '../../services/parser.service';
import { FavoriteService } from '../../services/favorite.service';
import { HistoryService } from '../../services/history.service';
import { SettingService } from '../../services/settings.service';


// Models
import { Show } from '../../models/show';
import { Film } from '../../models/film';
import { TVShow } from '../../models/tv_show';
import { Season } from '../../models/season';
import { Episode } from '../../models/episode';
import { ToastService } from '../../services/toast.service';
import { Video } from '../../models/video';

// Third party plugins
import { YoutubeVideoPlayer } from '@ionic-native/youtube-video-player';
import { StreamingMedia } from '@ionic-native/streaming-media';
import { Clipboard } from '@ionic-native/clipboard';
import { InAppBrowser } from '@ionic-native/in-app-browser';

@Component({
  selector: 'page-detail',
  templateUrl: 'detail.html',
  providers: [PizzaServerService, ToastService, ParserService, FavoriteService, HistoryService, SettingService]
})
export class DetailPage implements OnInit {
  @ViewChild(Content) content: Content;

  public show: Show;
  public reccomandedShows: Show[];
  public detail: Film | TVShow;
  public segment: string = "watch";

  spoiler: boolean;
  toolbar: boolean;
  target: string;
  overviewLimit: number = 200;
  overviewShow: string;
  coverImage: string;
  simplifiedInterface: boolean;

  // Loading
  loading: boolean;
  loadingMessage: Loading;

  // Videos and extractions
  videos: Video[];
  tmpurlsrc: string;
  selectedVideo: Video;

  // TVShow specific attributes
  selectedSeason: Season;
  selectedEpisode: Episode;
  lastSeenEpisode: Episode;

  constructor(public navCtrl: NavController, public navParams: NavParams,
              private pizzaService: PizzaServerService, public toastService: ToastService,
              private youtube: YoutubeVideoPlayer, private platform: Platform,
              public actionSheetCtrl: ActionSheetController,
              private parserService: ParserService, private streamingMedia: StreamingMedia,
              private clipboard: Clipboard, public loadingCtrl: LoadingController,
              private favoriteService: FavoriteService, private historyService: HistoryService,
              private settingService: SettingService,
              private iab: InAppBrowser) {
    // pass
  }

  async ngOnInit() {
    this.show = this.navParams.get('show');
    this.target = this.navParams.get('target');
    this.coverImage = this.show.backdrop_path;
    this.overviewShow = this.show.overview;
    this.simplifiedInterface = await this.settingService.getSetting('simplifiedInterface');

    }

  // Runs when the page is about to enter and become the active page.
  async ionViewWillEnter(): Promise<void> {
    this.show = this.navParams.get('show');
    this.target = this.navParams.get('target');
    this.spoiler = await this.settingService.getSetting('spoiler');
    this.toolbar = await this.settingService.getSetting('toolbar');
    this.segment = "watch";

    // Load details
    if (this.detail == null){
      try {
        this.detail = await this.pizzaService.getInfo(this.target, this.show.id);
        this.detail.favorite = await this.favoriteService.searchFavorite(this.show.id, this.target);
        this.detail.title = this.show.title;
      } catch (error) {
        console.log("error", error);
      }
    }

    // Load simplified interface setting
    this.reccomandedShows = await this.pizzaService.getTopList(this.target, 1, this.show.id);
    console.log(this.reccomandedShows);

    // If it is a movie, start checking for the links
    if (this.target === 'movie' && !this.videos){
      this.loading = true;
      try {
        let videos = await this.pizzaService.getMovies(this.show.id, this.show.title);
        let message: string = (videos.length == 0) ? "Nessun video trovato" : "Lista scaricata";
        this.toastService.showTextToast(message);
        this.videos = videos;
        this.scrollTo('vidBox');
      } catch (error) {
        let message: string = "Errore di connessione al server"
        this.toastService.showTextToast(message);
      } finally {
        this.loading = false;
      }
    }

    // Load last episode seen
    if (this.target === 'tv'){
      this.lastSeenEpisode = await this.historyService.getLastEpisode(this.show.id, this.target);
    }
  }

  ionViewWillLeave() {
    console.log("before leave");
    console.log(this.loadingMessage);
    if (this.loadingMessage) this.loadingMessage.dismissAll();
  }

  async selectNextEpisode(): Promise<void>  {
    this.loadingMessage = this.loadingCtrl.create({
          content: "Caricando il prossimo episodio da vedere..."
    });
    this.loading = true;
    this.loadingMessage.present();
    // Get next episode from storage
    let currentEpisode: Episode = await this.historyService.getLastEpisode(this.show.id, this.target);
    if (currentEpisode) {
      // If current episode exists
      // Load information about the season
      let nextEpisode: Episode = await this.getNextEpisode(currentEpisode);

      if (nextEpisode == null){
        // no next episode found
        // show a message and exit
        this.loadingMessage.dismiss();
        this.toastService.showTextToast('Prossimo episodio assente');
        return;
      }

      // Set global variables
      this.selectedSeason = await this.pizzaService.getSeasonInfo(this.show.id, nextEpisode.season_number);
      this.selectedEpisode = nextEpisode;
    } else {
      // There is no currentEpisode saved
      // Get the first season and the first episodes
      let seasonInfo: Season = await this.pizzaService.getSeasonInfo(this.show.id, 1);
      this.selectedSeason = seasonInfo;
      this.selectedEpisode = this.selectedSeason.episodes[0];
    }
    this.loadingMessage.dismiss();
    this.toastService.showTextToast('Sto cercando episodio S' + this.selectedEpisode.season_number + 'E' + this.selectedEpisode.episode_number);
    this.changeEpisode();
  }

  async selectCurrentEpisode(): Promise<void>  {
    let loadingMessage = this.loadingCtrl.create({
          content: "Caricando l'ultimo episodio visto..."
    });
    loadingMessage.present();
    // Get next episode from storage
    let currentEpisode: Episode = await this.historyService.getLastEpisode(this.show.id, this.target);
    if (currentEpisode) {
      // If current episode exists
      // Load information about the season
      let seasonInfo: Season = await this.pizzaService.getSeasonInfo(this.show.id, currentEpisode.season_number);
      // Set global variables
      this.selectedSeason = seasonInfo;
      this.selectedEpisode = currentEpisode;
    } else {
      // There is no currentEpisode saved
      // Get the first season and the first episodes
      let seasonInfo: Season = await this.pizzaService.getSeasonInfo(this.show.id, 1);
      this.selectedSeason = seasonInfo;
      this.selectedEpisode = this.selectedSeason.episodes[0];
    }
    loadingMessage.dismiss();
    this.toastService.showTextToast('Sto cercando episodio S' + this.selectedEpisode.season_number + 'E' + this.selectedEpisode.episode_number);
    this.changeEpisode();
  }

  scrollTo(element:string) {
    let el: any = document.getElementById(element);
    let yOffset = el.offsetTop;
    this.content.scrollTo(0, yOffset, 1000)
  }

  openYoutubeVideo(): void {
    let film = this.detail as Film;
    let videoId: string = film.videos[0].id;
    this.youtube.openVideo(videoId);
  }

  changeSeason(): void {
    this.loading = true;
    this.pizzaService.getSeasonInfo(this.show.id, this.selectedSeason.season_number).then(season => {
      this.selectedSeason.episodes = season.episodes;
      this.loading = false;
    });
  }

  goToDetail(selectedShow: Show): void {
    this.navCtrl.push(DetailPage, {show: selectedShow, target: this.target});
  }

  changeEpisode(): void {
    this.videos = [];
    this.coverImage = this.selectedEpisode.backdrop_path;
    this.overviewShow = (this.spoiler == true && this.selectedEpisode.overview.length > 0) ? this.selectedEpisode.overview : this.show.overview;
    this.loading = true;
    this.pizzaService.getTvShow(this.show.id, this.selectedSeason.season_number, this.selectedEpisode.episode_number).then(episodes => {
      let message: string = (episodes.length == 0) ? "Nessun video trovato" : "Lista scaricata";
      this.toastService.showTextToast(message);
      this.videos = episodes;
      this.loading = false;
      this.scrollTo('vidBox');
    }).catch(error  => {
      this.loading = false;
      let message: string = "Errore di connessione al server"
      this.toastService.showTextToast(message);
    });
  }

  async getNextEpisode(currentEpisode: Episode): Promise<Episode> {
    // Get season info
    let episodeSeason: Season = await this.pizzaService.getSeasonInfo(this.show.id, currentEpisode.season_number);
    // If season contains other episode, return next episode in the list
    if (episodeSeason.episodes.length > currentEpisode.episode_number) return episodeSeason.episodes[currentEpisode.episode_number];
    else {
      // Episode is the last episode of the season
      // Get next season number
      let seasonNumber = episodeSeason.season_number;
      let nextSeason = (this.detail as TVShow).seasons.find(x  => x.season_number == seasonNumber + 1);
      if (nextSeason) {
        // if exists a new season, get episodes of new season
        episodeSeason = await this.pizzaService.getSeasonInfo(this.show.id, seasonNumber + 1);
        // return first episode of the new season
        return episodeSeason.episodes[0];
      } else {
        return null; // no next episode found
      }
    }
  }

  saveOnHistory(): void {
    this.historyService.saveEpisode(this.show.id, this.target, this.selectedEpisode);
  }

  openMenu(url: string) {
    console.log('azioni per', url)
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Azioni',
      cssClass: 'action-sheets',
      buttons: [
        {
          text: 'Riproduci',
          icon: !this.platform.is('ios') ? 'play' : null,
          handler: () => {
            if (this.target == 'tv') this.saveOnHistory();
            this.lastSeenEpisode = this.selectedEpisode;
            this.historyService.saveLastShow(this.show.id, this.target);
            this.streamingMedia.playVideo(url);
          }
        },
        {
          text: 'Apri esternamente',
          icon: !this.platform.is('ios') ? 'open' : null,
          handler: () => {
            if (this.target == 'tv') this.saveOnHistory();
            this.lastSeenEpisode = this.selectedEpisode;
            this.historyService.saveLastShow(this.show.id, this.target);
            this.iab.create(url,'_system','clearcache=yes');
          }
        },
        {
          text: 'Copia',
          icon: !this.platform.is('ios') ? 'clipboard' : null,
          handler: () => {
            this.toastService.showTextToast('Copiato')
            this.clipboard.copy(url);
          }
        },
        {
          text: 'Annulla',
          role: 'cancel', // will always sort to be on the bottom
          icon: !this.platform.is('ios') ? 'close' : null,
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    actionSheet.present();
}

  changeFavorite(): void {
    if (this.detail.favorite){
      // Show is already on favorite list => remove it
      this.favoriteService.deleteFavorite(this.detail.id, this.target);
      this.detail.favorite = false;
      this.toastService.showTextToast('Rimosso dai preferiti', 'success');
    } else {
      // Add
      this.show.target = this.target;
      this.favoriteService.addFavorite(this.show);
      this.detail.favorite = true;
      this.toastService.showTextToast('Aggiunto ai preferiti', 'success');
    }
  }

  openVideoOptions(video: Video): void {
    this.selectedVideo = video;
    if (video.parsedUrl) {
      this.openMenu(video.parsedUrl);
    } else {
      this.parseUrl(video.originalUrl);
    }
  }

  // ###
  // ### Parsing methods
  // ###

  parseUrl(destUrl: string): void {
    let promiseUrl: Promise<string>;
    this.loadingMessage = this.loadingCtrl.create({
          content: 'Estraendo...'
    });
    this.loadingMessage.present();

    if (destUrl.includes('openload') || destUrl.includes('oload') || destUrl.includes('streamango')){
      // start iframe loading
      this.tmpurlsrc = destUrl;

      // get document promise
      let documentPromise = new Promise(function(resolve,reject){
        let el: any = document.getElementById('innerframetmp');
        el.addEventListener('load', function(){
          resolve(el.contentDocument);
        }, {once:true})
      });

      promiseUrl = documentPromise.then(doc => {
        let innerDoc = doc as Document;
        return this.parserService.extractUrl(this.selectedVideo.originalUrl, innerDoc);
      });

    } else {
      // handle rapidvideo
      promiseUrl = this.parserService.extractUrl(destUrl);
    }

    promiseUrl.then(parsedUrl => {
      this.toastService.showTextToast('Fatto!', 'success');
      this.selectedVideo.parsedUrl = parsedUrl;
      this.openMenu(parsedUrl);
      this.tmpurlsrc = 'about:blank';
      this.loadingMessage.dismiss();
    }).catch(error => {
      this.tmpurlsrc = 'about:blank';
      this.toastService.showTextToast('Non riesco ad estrarre', 'error');
      console.log('error', error);
      this.loadingMessage.dismiss();
    });
  }
}
