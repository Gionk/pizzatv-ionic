import { Component } from '@angular/core';
import { NavController, AlertController } from 'ionic-angular';
import { ToastService } from '../../services/toast.service';
import { SettingService } from '../../services/settings.service';


@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html',
  providers: [ToastService, SettingService]
})
export class SettingsPage {

  baseurl: string;
  fastSearch: boolean;
  showReccomanded: boolean;
  simplifiedInterface: boolean;
  spoiler: boolean;
  toolbar: boolean;
  numElement: number;
  useCache: boolean;

  constructor(public navCtrl: NavController, public toastService: ToastService,
              public settingService: SettingService, private alertCtrl: AlertController) {
  }

  // Runs when the page is about to enter and become the active page.
  ionViewWillEnter(): void {
    this.settingService.getSetting('baseurl').then(baseurl  => this.baseurl = baseurl);
    if (this.baseurl == 'false') this.baseurl  = "";
    this.settingService.getSetting('fastSearch').then(fastSearch  => this.fastSearch = fastSearch);
    this.settingService.getSetting('useCache').then(useCache  => this.useCache = useCache);
    this.settingService.getSetting('simplifiedInterface').then(simplifiedInterface  => this.simplifiedInterface = simplifiedInterface);
    this.settingService.getSetting('spoiler').then(spoiler  => this.spoiler = spoiler);
    this.settingService.getSetting('toolbar').then(toolbar  => this.toolbar = toolbar);
    this.settingService.getSetting('numElement').then(numElement  => this.numElement = numElement);
    this.settingService.getSetting('showReccomanded').then(showReccomanded  => this.showReccomanded = showReccomanded);
  }

  changeSpoiler(item: boolean) : void {
    if(item){
      let alert = this.alertCtrl.create({
        title: 'Spoiler',
        subTitle: 'Con tale modifica, quando si sceglierà un episodio di una serie tv, se essa contiene un descrizione sarà visuallizata al posto della descrizione classica della serie tv. Avvertimento: Alcune volte la descrizione potrebbe essere in inglese.',
        buttons: ['OK']
      });
      alert.present();
    }
  }

  changeElement() : void {
      let alert = this.alertCtrl.create({
        title: 'Numero di Elementi',
        subTitle: 'Dopo il salvataggio conviene riavviare app, per non avere problemi di configurazione.',
        buttons: ['OK']
      });
      alert.present();
  }

  isURL(url: string): boolean {
    let regex = /(http|https):\/\/(\w+:{0,1}\w*)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%!\-\/]))?/;
    let isValid: boolean = regex.test(url);
    return (isValid && !url.endsWith('/'))
  }

  save(): void {
    if (this.simplifiedInterface) this.showReccomanded = false;
    this.settingService.setSetting('fastSearch', this.fastSearch);
    this.settingService.setSetting('showReccomanded', this.showReccomanded);
    this.settingService.setSetting('useCache', this.useCache);
    this.settingService.setSetting('simplifiedInterface', this.simplifiedInterface);
    this.settingService.setSetting('spoiler', this.spoiler);
    this.settingService.setSetting('toolbar', this.toolbar);
    this.settingService.setSetting('numElement', this.numElement);
    if (this.baseurl && this.isURL(this.baseurl)){
      this.toastService.showTextToast('Impostazioni salvate');
      this.settingService.setSetting('baseurl', this.baseurl);
    } else {
      // alert
      let alert = this.alertCtrl.create({
        title: 'URL non corretta',
        subTitle: 'Devi inserire una URL valida',
        buttons: ['OK']
      });
      alert.present();
    }
  }
}
