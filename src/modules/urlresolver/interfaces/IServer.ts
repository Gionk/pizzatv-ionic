export interface IServer {
    extractUrl(string, Document?) : Promise<string>;
}