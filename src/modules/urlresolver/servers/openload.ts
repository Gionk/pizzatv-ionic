import { IServer } from '../interfaces/IServer'

export class Openload implements IServer {
    constructor(){
    }

    extractUrl(url: string, doc: Document) : Promise<string>{

        // HTML document to string
        let markup = doc.documentElement.innerHTML;

        // Get the videoId from url
        let parts = url.split("/");
        let videoId = parts[4];
        console.log("videoId", videoId);

        // Define the regex
        let escapedId = videoId.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
        let regex = new RegExp("(" + escapedId + "~.*)<");

        // Find a match
        let match = regex.exec(markup);

        // Build the extracted link
        let extracted = 'https://openload.co/stream/' + match[1];

        console.log("openload extracted", extracted);

        return Promise.resolve(extracted);
    }
}