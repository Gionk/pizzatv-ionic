import { IServer } from '../interfaces/IServer'


export class Streamango implements IServer {
    constructor(){        
    }

    async getEmbeddedUrl(url: string) : Promise<string> {
        if (!url.includes('embed')){
            // match something like .../f/video_id/ or .../d/video_id/something 
            let match = /\/.\/([^\/]+)/.exec(url)
            if (match) {
                let video_id = match[1];
                url = 'https://streamango.com/embed/' + video_id + '/';                             
            } else {
                // if there is not /f/ or /d/ or another schema
                // it just use the last part of the url
                // that hopefully is the video_id
                let pieces = url.split('/');
                let video_id = pieces.pop();
                url = 'https://streamango.com/embed/' + video_id + '/';                             
            }
        }

        return url;
    }

    async extractUrl(url: string, doc: Document) : Promise<string> {
        console.log(url);
        let videoNode = doc.getElementById("mgvideo_html5_api");
        let videoUrl = videoNode.getAttribute("src");
        if (!videoUrl.includes('http')){
            videoUrl = 'https:' + videoUrl;
        }

        console.log("extracted stream", videoUrl);
        return Promise.resolve(videoUrl);
    }
}