import { Http } from '@angular/http';
import { IServer } from '../interfaces/IServer'

export class Rapidvideo implements IServer {
    constructor(private http: Http){        
    }

    extractUrl(url: string) : Promise<string>{
        console.log('rapidvideo url', url);
        return this.http.get(url)
            .toPromise()
            .then(response => {
                let reg : RegExp = /"file":"([^"]+.mp4)"/g;
                let results = [];
                let match : RegExpExecArray = null;
                let content: string = response.text(); // to make regex works on document
                while ((match = reg.exec(content)) !== null) {
                    results.push(match[1]);
                }

                if (results.length == 0){
                    reg = /src="([^"]+.mp4)"/g;
                    while ((match = reg.exec(content)) !== null) {
                        results.push(match[1]);
                    }                    
                }

                let bestResolution : string = results.pop();
                return bestResolution.replace(/\\/g, '');
            });
    }
}