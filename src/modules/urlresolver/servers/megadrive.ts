import { Http } from '@angular/http';
import { IServer } from '../interfaces/IServer'


export class Megadrive implements IServer {
    constructor(private http: Http){
    }

    async extractUrl(url: string) : Promise<string>{
        let ANON = /^function[^{]*\{\s*|\s*\}$/g;
        let response =  await this.http.get(url).toPromise();
        let content: string = response.text(); // to make regex works on document
        let toUnpack = /(eval.function.p,a,c,k,e,.*?)\s*<\/script>/g.exec(content)[1];
        let s = eval(String(toUnpack.replace('eval', "")));
        let unpacked = String(s).replace(ANON, "");
        let videoUrl = /file"?\s*:\s*"([^"]+)",/g.exec(unpacked)[1];
        return videoUrl;
    }
}
