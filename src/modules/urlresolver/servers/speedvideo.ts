import { Http } from '@angular/http';
import { IServer } from '../interfaces/IServer'

export class Speedvideo implements IServer {
    constructor(private http: Http){        
    }

    extractUrl(url: string) : Promise<string>{
        console.log('speedvideo url', url);
        return this.http.get(url)
            .toPromise()
            .then(response => {
                let reg : RegExp = /https?:\/\/speedvideo[^('+")]+mp4[^('+")]+/g;
                let results = [];
                let match : RegExpExecArray = null;
                let content: string = response.text(); // to make regex works on document
                
                while ((match = reg.exec(content)) !== null) {
                    results.push(match[0]);
                }

                return results[0];
            });
    }
}