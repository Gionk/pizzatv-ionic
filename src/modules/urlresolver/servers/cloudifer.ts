import { Http } from '@angular/http';
import { IServer } from '../interfaces/IServer'


export class Cloudifer implements IServer {
    constructor(private http: Http){
    }

    async extractUrl(url: string) : Promise<string>{
        console.log('cloudifer url', url)
        let response =  await this.http.get(url).toPromise();
        let content: string = response.text(); // to make regex works on document
        let video_url = /file: "([^"]+)"/g.exec(content)[1]
        if (!video_url.includes('http')){
            video_url = 'http:' + video_url;
        }
        return video_url;
    }
}
