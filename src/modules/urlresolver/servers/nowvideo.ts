import { Http } from '@angular/http';
import { IServer } from '../interfaces/IServer'


export class Nowvideo implements IServer {
    constructor(private http: Http){
    }

    async extractUrl(url: string) : Promise<string>{
        console.log(url);
        url = url.replace("http://www.nowvideo.li/video/", "http://www.nowvideo.li/embed/?v=")
        console.log(url);

        let response =  await this.http.get(url).toPromise();
        let content: string = response.text(); // to make regex works on document

        let video_url = /<source src=[\'"]([^\'"]+)[\'"]/g.exec(content)[1];

        if (video_url.includes('.mpd')){
            let video_id = /\/dash\/(.*?)\//g.exec(content)[1];
            video_url = "http://www.nowvideo.li/download.php%3Ffile=mm" + video_id + ".mp4";
            video_url = video_url.replace('\/dl(\d)*\/', '\/dl\/')
            video_url = video_url.replace("%3F", "?")
        }


        if (!video_url.includes('http')){
            video_url = 'http:' + video_url;
        }

        console.log(video_url);
        return video_url;
    }
}
