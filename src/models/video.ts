export interface Video {
    channel: string;
    originalUrl: string;
    parsedUrl: string;
    server: string;
}