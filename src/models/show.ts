export class Show {
    id: string;
    title: string;
    overview: string;
    poster_path: string;
    poster_path_thumbnail: string;
    backdrop_path: string;
    backdrop_path_thumbnail: string;
    popularity: number;
    target: string;
    favorite: boolean;
}
