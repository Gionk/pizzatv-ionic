export class YoutubeVideo {
    url: string;
    id: string;
    title: string;
}