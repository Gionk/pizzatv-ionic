import { Show } from './show';
import { YoutubeVideo } from './youtube_video';

export class Film extends Show {
    videos: YoutubeVideo[];
}