import { Injectable } from '@angular/core';
import { Http, RequestOptions, URLSearchParams } from '@angular/http';
import { Show } from '../models/show';
import { Film } from '../models/film';
import { TVShow } from '../models/tv_show';
import { Season } from '../models/season';
import { Video } from '../models/video';
import { SettingService } from '../services/settings.service';

import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/timeout';

@Injectable()
export class PizzaServerService {

    constructor(private http: Http, private settingService: SettingService){
    }

    async getTopList(target: string, page: number, showId?: string): Promise<Show[]> {
        let baseUrl: string = await this.settingService.getSetting('baseurl');
        let topListUrl = baseUrl + "/toplist";
        let params: URLSearchParams = new URLSearchParams();

        if (showId) params.set('showId', showId);
        params.set('target', target);
        params.set('page', String(page));
        
        let requestOptions = new RequestOptions();
        requestOptions.search = params;
        
        let response = await this.http.get(topListUrl, requestOptions).timeout(3000).toPromise();
        return response.json() as Show[];
    }

    async search(title: string, target: string): Promise<Show[]> {
        let baseUrl: string = await this.settingService.getSetting('baseurl');
        let params: URLSearchParams = new URLSearchParams();
        params.set('title', title);
        params.set('target', target);
        let requestOptions = new RequestOptions();
        requestOptions.search = params;
        let searchUrl = baseUrl + "/search";
        let response = await this.http.get(searchUrl, requestOptions).toPromise();
        return response.json() as Show[];
    }

    async getInfo(target: string, showId: string): Promise<Film | TVShow> {
        let baseUrl: string = await this.settingService.getSetting('baseurl');        
        let params: URLSearchParams = new URLSearchParams();
        
        params.set('target', target);
        params.set('showId', showId);

        let requestOptions = new RequestOptions();
        requestOptions.search = params;
        let infoUrl = baseUrl + "/info";

        // Ugly but works
        let response = await this.http.get(infoUrl, requestOptions).toPromise();
        if (target == 'tv'){
            return response.json().infoLabels as TVShow;
        } else {
            return response.json().infoLabels as Film;
        }
    }

    async getSeasonInfo(showId: string, season: number) : Promise<Season> {
        let baseUrl: string = await this.settingService.getSetting('baseurl');        
        let params: URLSearchParams = new URLSearchParams();
        params.set('showId', showId);
        params.set('season', season.toString());
        let requestOptions = new RequestOptions();
        requestOptions.search = params;
        let infoUrl = baseUrl + "/seasoninfo";

        let response = await this.http.get(infoUrl, requestOptions).toPromise();
        return response.json().seasonInfo as Season;
    }

    async getTvShow(showId: string, season: number, episode: number) : Promise<Video[]>{
        let baseUrl: string = await this.settingService.getSetting('baseurl');  
        let useCache: boolean = await this.settingService.getSetting('useCache');
        let useCacheStr: string = String(useCache);     
        let params: URLSearchParams = new URLSearchParams();
        params.set('showId', showId);
        params.set('season', season.toString());
        params.set('episode', episode.toString());
        params.set('cache', useCacheStr);
        let requestOptions = new RequestOptions();
        requestOptions.search = params;
        let infoUrl = baseUrl + "/tv";

        let response = await this.http.get(infoUrl, requestOptions).toPromise();
        return response.json().videos as Video[];
    }

    async getMovies(showId: string, title: string){
        let baseUrl: string = await this.settingService.getSetting('baseurl');        
        title = title.replace(" ", "+");
        let params: URLSearchParams = new URLSearchParams();
        params.set('showId', showId);
        params.set('title', title);
        let requestOptions = new RequestOptions();
        requestOptions.search = params;
        let infoUrl = baseUrl + "/movie";

        let response = await this.http.get(infoUrl, requestOptions).toPromise();
        return response.json().videos as Video[];
    };
}