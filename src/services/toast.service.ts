import { Injectable } from '@angular/core';
import { ToastController } from 'ionic-angular';

@Injectable()
export class ToastService {
    constructor(private toastCtrl: ToastController){
    }

    // To Show a String as Toast Notification
    showTextToast(title: string, toastType?: string) {
        let toast: any;
        if (toastType) {
            let cssStyle = toastType + 'Toast';
            toast = this.toastCtrl.create({
                message: title,
                cssClass: cssStyle,
                duration: 3000,
                position: 'top',
                showCloseButton: true
            });
        } else {
            toast = this.toastCtrl.create({
                message: title,
                cssClass: "normalToast",
                duration: 3000,
                position: 'top',
                showCloseButton: true
            });
        }
        toast.present();
    }
}
