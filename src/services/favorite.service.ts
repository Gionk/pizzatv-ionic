import { Injectable } from '@angular/core';
import { Show } from '../models/show';
import { Storage } from '@ionic/storage';

@Injectable()
export class FavoriteService {
    constructor(private storage: Storage){       
    }

    async getFavoriteList(): Promise<Show[]> {
        let favoriteList: Show[] = await this.storage.get('favorite');
        if (!favoriteList){
            // Create a new lists
            favoriteList = [];
            this.storage.set('favorite', []);
        }
        return favoriteList;
    }

    async addFavorite(show: Show): Promise<void> {
        let favoriteList: Show[] = await this.getFavoriteList();
        favoriteList.push(show);
        this.storage.set('favorite', favoriteList);
    }

    async deleteFavorite(showId: string, target: string): Promise<void> {
        let favoriteList: Show[] = await this.getFavoriteList();
        let filtered = favoriteList.filter(x => !(x.id == showId && x.target == target));
        this.storage.set('favorite', filtered);
    }

    async searchFavorite(showId: string, target: string): Promise<boolean> {
        let favoriteList: Show[] = await this.getFavoriteList();
        let filtered = favoriteList.filter(x => x.id == showId && x.target == target);
        return filtered.length > 0;
    }
}