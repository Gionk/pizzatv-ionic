import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Rapidvideo } from '../modules/urlresolver/servers/rapidvideo'
import { Openload } from '../modules/urlresolver/servers/openload'
import { Nowvideo } from '../modules/urlresolver/servers/nowvideo'
import { Wstream } from '../modules/urlresolver/servers/wstream'
import { Cloudifer } from '../modules/urlresolver/servers/cloudifer'
import { Megadrive } from '../modules/urlresolver/servers/megadrive'
import { Streamango } from '../modules/urlresolver/servers/streamango'
import { Speedvideo } from '../modules/urlresolver/servers/speedvideo'

import 'rxjs/add/operator/toPromise';

@Injectable()
export class ParserService {
    constructor(private http: Http){        
    }

    extractUrl(url: string, param?: Document): Promise<string> {
        if (url.includes('openload') || url.includes('oload')){
            let doc: Document = param as Document;
            let openloadService = new Openload();
            return openloadService.extractUrl(url, doc);
        } else if (url.includes('rapidvideo')) {
            let rapidvideoService = new Rapidvideo(this.http);
            return rapidvideoService.extractUrl(url);
        } else if (url.includes('wstream')) {
            let wService = new Wstream(this.http);
            return wService.extractUrl(url);            
        } else if (url.includes('cloudifer')) {
            let cloudiferService = new Cloudifer(this.http);
            return cloudiferService.extractUrl(url);            
        } else if (url.includes('nowvideo')) {
            let nowService = new Nowvideo(this.http);
            return nowService.extractUrl(url);            
        } else if (url.includes('megadrive')) {
            let megadriveService = new Megadrive(this.http);
            return megadriveService.extractUrl(url);            
        } else if (url.includes('streamango')) {
            let doc: Document = param as Document;
            let streamangoService = new Streamango();
            return streamangoService.extractUrl(url, doc);            
        } else if (url.includes('speedvideo')) {
            let streamangoService = new Speedvideo(this.http);
            return streamangoService.extractUrl(url);            
        }  else {
            throw new URIError('unhandledUri');   
        }
    }
}